pub fn add_two(a: i32) -> i32 {
    a + 2
}

pub struct Cpu {
    acc: u8,
    x: u8,
    pc: u8,
}

impl Cpu {
    pub fn new() -> Cpu {
        Cpu {
            acc: 0,
            x: 0,
            pc: 0,
        }
    }

    pub fn lda(&mut self, input: u8) {
        self.acc = input
    }

    pub fn ldx(&mut self, input: u8) {
        self.x = input
    }

    pub fn step(&mut self) {
        self.pc += 1
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn load_value_into_acc() {
        let mut cpu = Cpu::new();
        cpu.lda(8);
        assert_eq!(8, cpu.acc);
    }

    #[test]
    fn load_value_into_x() {
        let mut cpu = Cpu::new();
        cpu.ldx(42);
        assert_eq!(42, cpu.x);
    }

    #[test]
    fn step() {
        let mut cpu = Cpu::new();
        assert_eq!(0, cpu.pc);
        cpu.step();
        assert_eq!(1, cpu.pc);
    }
}
